<?php

use yii\db\Migration;

class m170520_104315_create_table_customers extends Migration
{
    public function up()
    {
        $this->createTable('customers', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'email' => $this->text(),
        ]);
    }

    public function down()
    {
          $this->dropTable('customers');

    }

}
