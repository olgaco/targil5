<?php

use yii\db\Migration;

/**
 * Handles adding description to table `customers`.
 */
class m170520_112814_add_description_column_to_customers_table extends Migration
{

    public function up()
    {
        $this->addColumn('customers', 'description', $this->text());
    }

    public function down()
    {
        $this->dropColumn('customers', 'description');
    }
}
